#!/usr/bin/env python
from video2dcm import video_to_dcm
from tempfile import NamedTemporaryFile
import subprocess as sp
import os
import sys

try:
    from subprocess import DEVNULL  # py3k
except ImportError:
    DEVNULL = open(os.devnull, 'wb')

DCMCJPEG_BINARY = 'dcmcjpeg'


def dcm_to_dcmjpg(dcm_file, dcmjpg_file):
    cmd = [DCMCJPEG_BINARY,
           '+eb',
           '+be',
           dcm_file,
           dcmjpg_file]

    proc = sp.Popen(cmd)
    proc.wait()


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("usage: video2dcmjpg VIDEO_FILE DCM_FILE")
        exit(1)

    VIDEO_FILE = sys.argv[1]
    DCM_FILE = sys.argv[2]

    with NamedTemporaryFile() as tmp_file:
        # write dcm to temporary file
        video_to_dcm(VIDEO_FILE, tmp_file)
        tmp_file.flush()

        # compress temporary file
        dcm_to_dcmjpg(tmp_file.name, DCM_FILE)
