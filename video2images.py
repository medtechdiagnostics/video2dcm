#!/usr/bin/env python
from PIL import Image
import moviepy.video.io.ffmpeg_reader as reader
import os
import errno
import sys


def make_sure_path_exists(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise


def video_to_images(input, output_pattern):
    video = reader.FFMPEG_VideoReader(filename=input)

    # store frames as images
    for index in range(video.nframes):
        image = video.read_frame()
        Image.fromarray(image).save(output_pattern.format(index=index))

    video.close()


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("usage: video2images VIDEO_FILE EXPORT_PATH")
        exit(1)

    VIDEO_FILE = sys.argv[1]
    EXPORT_PATH = sys.argv[2]

    make_sure_path_exists(EXPORT_PATH)
    video_to_images(VIDEO_FILE, EXPORT_PATH + '/image{index}.jpg')
