#!/usr/bin/env python
import moviepy.video.io.ffmpeg_reader as reader
import datetime
import sys
from pydicom.dataset import Dataset, FileDataset


def video_to_dcm(video_file, dcm_file):
    # Ultrasound Multiframe Image Storage
    metadata = Dataset()
    metadata.MediaStorageSOPClassUID = '1.2.840.10008.5.1.4.1.1.3.1'
    metadata.MediaStorageSOPInstanceUID = '1.2.840.113663.1500.1.315234023.3.10.20161208.133829.671'
    metadata.ImplementationClassUID = '1.2.840.113663.1500.1.315234023.3.10.20161208.133829.671'

    # create empty DICOM file with preset metadata
    ds = FileDataset(dcm_file, {}, file_meta=metadata, preamble=b"\0" * 128)

    # Set the transfer syntax
    ds.is_little_endian = True
    ds.is_implicit_VR = True

    # configure for ultrasound data
    ds.Modality = "US"
    ds.ImageType = ['DERIVED', 'PRIMARY', 'VASCULAR']
    ds.SeriesDescription = 'UA Lower Limb Deep Veins'
    ds.UltrasoundColorDataPresent = 0

    # patient data
    ds.PatientName = "Anonymous"
    ds.PatientID = '123456'

    # Set creation date/time
    dt = datetime.datetime.now()
    ds.ContentDate = dt.strftime('%Y%m%d')
    # long format with micro seconds
    timeStr = dt.strftime('%H%M%S.%f')
    ds.ContentTime = timeStr

    # load video file and store 1 frame
    video = reader.FFMPEG_VideoReader(filename=video_file)
    width, height = video.size

    # Pixel Data Information
    ds.PhotometricInterpretation = 'RGB'
    ds.PlanarConfiguration = 0  # Interlaced Channels
    ds.BitsAllocated = 8
    ds.BitsStored = 8
    ds.HighBit = 7
    ds.PixelRepresentation = 0  # Unsigned Bits
    ds.SamplesPerPixel = video.depth  # 3 Channel
    ds.Rows = height
    ds.Columns = width
    ds.NumberOfFrames = video.nframes

    # read every frame into a buffer
    buf = bytearray()
    for index in range(video.nframes):
        image = video.read_frame().tobytes()
        buf.extend(image)

    # clear video buffer
    video.close()

    # store mutable bytesarray as immutable bytes string
    ds.PixelData = bytes(buf)

    # save DICOM with pixel data
    ds.save_as(dcm_file)


if __name__ == "__main__":
    if len(sys.argv) < 3:
        print("usage: video2dcm VIDEO_FILE DCM_FILE")
        exit(1)

    VIDEO_FILE = sys.argv[1]
    DCM_FILE = sys.argv[2]

    video_to_dcm(VIDEO_FILE, DCM_FILE)
