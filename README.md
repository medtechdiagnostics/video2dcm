# Video to DICOM converter

This is a simple tool which converts videos of ultrasound imaging to
[DICOM](http://en.wikipedia.org/wiki/DICOM) multi-frame sequences. The DICOM format is configured to
vascular, non-colored ultrasound examination. The tool is based on
[ffmpeg](http://ffmpeg.org), so we support its common video codecs.

## Installation and Requirements

1. Download [ffmpeg](https://ffmpeg.org/download.html) or use homebrew (`brew install ffmpeg`).
2. Fork and install our package.

```bash
git clone http://github.com/medtechdiagnostics/video2dcm
cd video2dcm
pip install -r ./requirements.txt
```

## Usage

```bash
# convert a video to a DICOM file
./video2dcm sample.wmv sample.dcm

# convert a video to a sequence of images
# and store into ./exports path
./video2images sample.wmv ./exports
```
